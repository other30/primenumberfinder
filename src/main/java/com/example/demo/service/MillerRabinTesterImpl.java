package com.example.demo.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Random;

/**
 * Реализация вероятностного полиномиального теста простоты Миллера — Рабина.
 * кол-во раундов задается в application.yml
 */
@Service
public class MillerRabinTesterImpl implements PrimeNumberTester {

    @Value("${app.millerRabinTester.round}")
    private int roundCount = 40;

    @Override
    public boolean isPrimeNumber(BigInteger number) {

        if (number.equals(BigInteger.TWO) || number.equals(BigInteger.valueOf(3))) {
            return true;
        }

        if (number.compareTo(BigInteger.TWO) < 0 || number.mod(BigInteger.TWO).equals(BigInteger.ZERO) ) {
            return false;
        }

        BigInteger t = number.subtract(BigInteger.ONE);
        BigInteger s = BigInteger.ZERO;

        while (t.mod(BigInteger.TWO).equals(BigInteger.ZERO)){
            t = t.divide(BigInteger.TWO);
            s = s.add(BigInteger.ONE);
        }

        for (int i = 0; i < roundCount; i++)
        {
            BigInteger a = getRandomBigInteger(BigInteger.TWO,number.subtract(BigInteger.TWO));
            BigInteger x = a.modPow(t, number);

            if (x.equals(BigInteger.ONE) || x.equals(number.subtract(BigInteger.ONE)) ) {
                continue;
            }

            for (BigInteger p = BigInteger.ONE; p.compareTo(s) < 0; p=p.add(BigInteger.ONE))
            {
                x = x.modPow( BigInteger.TWO, number);
                if (x.equals(BigInteger.ONE))
                    return false;

                if (x.equals(number.subtract(BigInteger.ONE)))
                    break;
            }

            if (!x.equals(number.subtract(BigInteger.ONE)))
                return false;
        }

        return true;
    }

    private BigInteger getRandomBigInteger(BigInteger minLimit, BigInteger maxLimit){
        BigInteger bigInteger = maxLimit.subtract(minLimit);
        if(bigInteger.equals(BigInteger.ZERO))
            return minLimit;
        Random randNum = new Random();
        int len = maxLimit.bitLength();
        BigInteger res = new BigInteger(len, randNum);
        if (res.compareTo(minLimit) < 0)
            res = res.add(minLimit);
        if (res.compareTo(bigInteger) >= 0)
            res = res.mod(bigInteger).add(minLimit);
        return res;
    }
}
