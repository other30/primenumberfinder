package com.example.demo.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

public interface PrimesFinder {
    /**
     * Поиск простых чисел в списке numbers
     * функция использует многопоточность, размер пула настраивается {@link com.example.demo.config.AppConfig}
     * @param numbers - список целых чисел
     * @param onlyDuplicate - поиск только дублирующихся чисел
     * @return - map простых чисел и их кол-во
     */
    Map<BigInteger, Integer> find(List<BigInteger> numbers ,Boolean onlyDuplicate);

}
