package com.example.demo.service;

import com.example.demo.config.AppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

@Service
@EnableConfigurationProperties(AppConfig.class)
public class PrimesFinderImpl implements PrimesFinder {

    private final PrimeNumberTester primeNumberTester;
    private final AppConfig appConfig;
    Logger logger = LoggerFactory.getLogger(PrimesFinderImpl.class);

    @Autowired
    public PrimesFinderImpl(
            PrimeNumberTester primeNumberTester,
            AppConfig appConfig) {
        this.primeNumberTester = primeNumberTester;
        this.appConfig = appConfig;
    }

    @Override
    public Map<BigInteger, Integer> find(List<BigInteger> numbers, Boolean onlyDuplicate) {
        Map<BigInteger, Integer> primeNumberMap = new ConcurrentHashMap<>();
        ForkJoinPool customThreadPool = new ForkJoinPool(appConfig.getThreadPoolFinder());

        try {
            customThreadPool.submit(() -> numbers
                    .parallelStream()
                    .forEach(e -> {
                                logger.debug("ThreadId : " + Thread.currentThread().getId());
                                if (primeNumberMap.get(e) != null || primeNumberTester.isPrimeNumber(e)) {
                                    addPrimeNumber(primeNumberMap, e);
                                }
                            }

                    )).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            customThreadPool.shutdown();
        }

        if (onlyDuplicate != null && onlyDuplicate) {
            return primeNumberMap.entrySet()
                    .stream()
                    .filter(x -> x.getValue() > 1)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        } else {
            return primeNumberMap;
        }
    }


    public void addPrimeNumber( Map<BigInteger, Integer> map,BigInteger primeNumber){
        map.merge(primeNumber, 1, Integer::sum);
    }
}
