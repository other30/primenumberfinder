package com.example.demo.service;

import java.math.BigInteger;

public interface PrimeNumberTester {
    boolean isPrimeNumber(BigInteger number);
}
