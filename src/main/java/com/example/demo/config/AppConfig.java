package com.example.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "app")
public class AppConfig {


    public int getThreadPoolFinder() {
        return threadPoolFinder;
    }

    public void setThreadPoolFinder(int threadPoolFinder) {
        this.threadPoolFinder = threadPoolFinder;
    }

    private int threadPoolFinder = 2;
}
