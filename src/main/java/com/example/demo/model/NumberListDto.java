package com.example.demo.model;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.List;

@Data
public class NumberListDto {
    @NotNull
    public List<@NotNull BigInteger> numbers;
    public Boolean onlyDuplicate;
}
