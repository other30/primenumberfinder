package com.example.demo.controller;

import com.example.demo.model.NumberListDto;
import com.example.demo.service.PrimesFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigInteger;
import java.util.Map;

@RestController
public class MainController {

    private final PrimesFinder primesFinder;

    @Autowired
    public MainController(PrimesFinder primesFinder) {
        this.primesFinder = primesFinder;
    }

    @RequestMapping(value = "/find/primes", method = RequestMethod.POST)
    public Map<BigInteger, Integer> findDuplicate(@RequestBody @Valid NumberListDto payload) {
        return primesFinder.find(payload.numbers,payload.onlyDuplicate);
    }


}
